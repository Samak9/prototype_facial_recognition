import json

config_file = "config.json"

class Config:

    # Fonction permettant de lire le fichier liée a la variable config_file
    @staticmethod
    def getConfig():
        with open(config_file, 'r') as f:
            config = json.load(f)
            return config

    # Recupere les configs liées aux images
    @staticmethod
    def getImageConfig(properties = ["height", "width", "channels", "extension"]):
        config = Config.getConfig()

        data = ()

        if "height" in properties:
            data += (config["Images"]["Property"]["height"],)

        if "width" in properties:
            data += (config["Images"]["Property"]["width"],)

        if "channels" in properties:
            data += (config["Images"]["Property"]["channels"],)

        if "extension" in properties:
            data = (config["Images"]["Property"]["extension"],)

        return data

    # recueperes les liens des dossiers ou se trouve les images
    @staticmethod
    def getImagePathConfig(properties = ["input", "tmp", "output", "dataset", "datasetTrain", "datasetTest", "predict"]):
        config = Config.getConfig()

        data = ()

        if "input" in properties:
            data += (config["Images"]["Paths"]["input"],)

        if "tmp" in properties:
            data += (config["Images"]["Paths"]["tmp"],)

        if "output" in properties:
            data += (config["Images"]["Paths"]["output"],)

        if "dataset" in properties:
            data += (config["Images"]["Paths"]["dataset"],)

        if "datasetTrain" in properties:
            data += (config["Images"]["Paths"]["datasetTrain"],)

        if "datasetTest" in properties:
            data += (config["Images"]["Paths"]["datasetTest"],)

        if "predict" in properties:
            data += (config["Images"]["Paths"]["predict"],)

        return data

    # Configuration pour l'IA
    @staticmethod
    def getDeepLearningConfig(properties = ["percentage_train_test", "model_dir"]):
        config = Config.getConfig()

        data = ()

        if "percentage_train_test" in properties:
            data += (config["Deeplearning"]["percentage_train_test"],)

        if "model_dir" in properties:
            data += (config["Deeplearning"]["model_dir"],)

        return data


    # Configuration pour la detection de tete
    @staticmethod
    def getDetectFacesConfig(properties = ["folder", "file"]):
        config = Config.getConfig()

        data = ()

        if "folder" in properties:
            data += (config["DetectFaces"]["cascadeFolder"],)

        if "file" in properties:
            data += (config["DetectFaces"]["cascadeFile"],)

        return data

    # Configuration pour le nom des fichiers enregister
    @staticmethod
    def getFilesConfig(properties=["folder", "images", "labels"]):
        config = Config.getConfig()

        data = ()

        if "folder" in properties:
            data += (config["Files"]["folder"],)

        if "images" in properties:
            data += (config["Files"]["gzipFileImages"],)

        if "labels" in properties:
            data += (config["Files"]["gzipFileLabels"],)

        return data
