# help source : https://github.com/kratzert/finetune_alexnet_with_tensorflow

import os
import sys

import tensorflow as tf
import random
import numpy as np

#from traiterImageUtil import TraiterImage

class ImageDataGenerator:


    def __init__(self):
        self.IMAGE_SIZE = 100
        self.CHANNELS = 1
        self.isWindowsSystem = "win" in sys.platform
        self.DOSSIER_SEPARATOR = "\\" if self.isWindowsSystem else "/"
        self.WORKING_DIRECTORY = os.path.dirname(os.path.realpath(__file__)) + self.DOSSIER_SEPARATOR
        self.INPUT = self.WORKING_DIRECTORY + "images_input" + self.DOSSIER_SEPARATOR
        self.TMP = self.WORKING_DIRECTORY + "tmp" + self.DOSSIER_SEPARATOR
        self.OUTPUT = self.WORKING_DIRECTORY + "output" + self.DOSSIER_SEPARATOR
        self.PERCENTAGE_TRAIN_VS_TEST = 80
        self.trainIterator = 0

        #self.formatImages()
        allImagesLabelsDictionnary = self.getAllImagesLabels(self.OUTPUT)

        self.trainImagesLabels, self.evalImagesLabels = self.splitTrainEval(allImagesLabelsDictionnary)

        self.numberTrainImagesLabels = len(self.trainImagesLabels)
        self.numberEvalImagesLabels = len(self.evalImagesLabels)
        #self.trainDataset = self.convertDicToTensorflow(imagesLabels)
        #self.evalDataset = self.convertDicToTensorflow(imagesLabelsEval)


    # Met les images dans le format attendu
    # GrayScale, Crop, Resize, Convertion en JPG
    def formatImages(self):
        traiterImage = TraiterImage(28,28)
        traiterImage.getFilesWithFaces(self.INPUT,self.OUTPUT)
        """
        command = "python ./requirement/resize_greyscale_crop_images/openCV2/script.py --input %s --tmp %s --output %s" % (self.INPUT,self.TMP,self.OUTPUT)

        # Obliger de la faire a la main, la variable ne marche pas
        if self.isWindowsSystem :
            command = command.replace("/","\\")
        #print("command : ", command)
        #exit()
        # lance la commande
        # EN COMMENTAIRE parce que ça sert a rien a de lance a chaque test mais a changer décommanté lorsque les test tensorflow seront fini
        #os.system(command)
        """
    # recupere les images
    def getAllImagesLabels(self,folder):
        #imagesPathArray = []
        #labelsArray = []
        self.folderArray = []
        self.imagesLabels = {}
        for dirPath, dirNames, files in os.walk(folder):
            for dirName in dirNames:
                if dirName.startswith("."):
                    continue

                if dirName not in self.folderArray:
                    self.folderArray.append(dirName)
                directory = dirPath + dirName

                for subDirPath, subDirNames, subFiles in os.walk(directory):
                    for file in subFiles:
                        if file.startswith("."):
                            continue
                        imagePath = subDirPath + self.DOSSIER_SEPARATOR + file
                        #imagesPathArray.append(imagePath)

                        label = int(dirName[1:]) # doit avoir un int

                        #labelsArray.append(label)
                        #imagesLabels[imagePath] = label
                        self.imagesLabels[imagePath] = dirName

        self.numberLabels = len(self.folderArray)

        # Save
        #print("imageLabels lenght : ", len(self.imagesLabels))
        #np.save(self.NUMPY_FILE, self.imagesLabels)

        #tmpSess = tf.Session()
        #print("\n")
        for key in self.imagesLabels:
            #print("# DEBUG : imagesLabels")
            #print("key : ", key)
            #print("value : ", imagesLabels[key])
            #index = self.folderArray.index(self.imagesLabels[key])
            #print("index : ", index)
            #label = self.getLabel(index)
            #print("label output : ", tmpSess.run(label))
            #self.imagesLabels[key] = label
            self.imagesLabels[key] = self.getLabelFromName(self.imagesLabels[key])

        #return imagesPathArray, labelsArray
        return self.imagesLabels

    """
    def loadImagesLabels():
        numpy_file = "numpy_file_save_imagesLabels.npy"
        ndarray = np.load(numpy_file)

        self.imagesLabels = dic(np.ndenumerate(ndarray))
    """

    def getLabelFromName(self,name):
        if name not in self.folderArray:
            return None

        index = self.folderArray.index(name)

        return index
        '''
        label = self.getLabel(index)
        #print("label output : ", tmpSess.run(label))
        #self.imagesLabels[key] = label
        return label
        '''

    def getLabel(self,index):
        #print("# DEBUG: GetLabel function")
        #print("GetLabel")
        #print("numberLabels : ", self.numberLabels)
        #print("index : ", index)
        arr = []
        for i in range(self.numberLabels):
            if i == index:
                arr.append(1)
            else :
                arr.append(0)
        #print("tableau : ", arr)
        #exit()
        #label = tf.convert_to_tensor(arr, dtype=tf.int32)
        #return label
        return arr


    # todo split les images train et test
    def splitTrainEval(self, imagesLabels):
        numberTrain = int( len(imagesLabels) * self.PERCENTAGE_TRAIN_VS_TEST / 100)

        imagesLabelsEval = {}

        for i in range(numberTrain, len(imagesLabels)):
            index = random.randint(0,len(imagesLabels)-1)

            imagePath, label = list(imagesLabels.items())[index]

            imagesLabelsEval[imagePath] = label

            del imagesLabels[imagePath]

        return imagesLabels, imagesLabelsEval

    # images en tensor pour le trainning
    def convertDicToTensorflow(self,dictionnary):
        imagesPathArray = list(dictionnary.keys())
        labelsArray = list(dictionnary.values())

        #files = tf.constant(imagesPathArray)
        #labels = tf.constant(labelsArray)
        files = tf.convert_to_tensor(imagesPathArray, dtype=tf.string)
        labels = tf.convert_to_tensor(labelsArray, dtype=tf.int32)
        dataset = tf.contrib.data.Dataset.from_tensor_slices((files, labels))
        #dataset = dataset.map(self._parse_function,output_buffer_size=self.batch_size)
        #dataset = dataset.batch(self.batch_size)
        dataset = dataset.shuffle(buffer_size=len(labelsArray))

        return dataset

    def _parse_function(self,filename, label):
        image_string = tf.read_file(filename)
        #print("\n\n\n\n")
        #print("channels : ", self.CHANNELS)
        image_decoded = tf.image.decode_png(image_string, channels=self.CHANNELS)
        #print("image_decoded : ", tf.shape(image_decoded[0][0]))
        image = tf.cast(image_decoded, tf.float32)

        #IMAGENET_MEAN = tf.constant([123.68, 116.779, 103.939], dtype=tf.float32)
        #image = tf.subtract(image, IMAGENET_MEAN)
        return image, label

    def getTrainDataSet(self):
        #return self.convertDicToTensorflow(self.trainImagesLabels)
        return self.trainImagesLabels

    def getEvalDataSet(self):
        #return self.convertDicToTensorflow(self.evalImagesLabels)
        return self.evalImagesLabels

    def getNumberTrain(self):
        return self.numberTrainImagesLabels

    def getNumberEval(self):
        return self.numberEvalImagesLabels

    def getNumberOutput(self):
        return self.numberLabels

    def getNameByLabel(self,label,sess=None):
        if sess is None:
            sess = tf.Session()
        expectedLabel = sess.run(label)
        expectedIndex = np.where(expectedLabel==1)[0][0]

        for name in self.folderArray:
            index = int(name[1:]) - 1
            label = sess.run(self.getLabel(index))

            npwhere = np.where(label==1)[0]
            if len(npwhere) > 0:
                currentIndex = npwhere[0]
            else:
                currentIndex = -1


            if currentIndex == expectedIndex:
                return name
