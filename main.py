#  Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
"""Convolutional Neural Network Estimator for MNIST, built with tf.layers."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf
from datetime import datetime

from config import Config
from imageUtil import ImageUtil
from model import Model
from convert_images_to_mnist_format import ConvertImageToMnistFormat
from custom_read_file import CustomReadFile

begin = datetime.now()
tf.logging.set_verbosity(tf.logging.INFO)

def main(unused_argv):
    """
    # PreProcessing --> garde que les images avec des tetes détecté
    input = Config.getImagePathConfig(["input"])[0]
    output = Config.getImagePathConfig(["output"])[0]
    ImageUtil().formatImages(input, output)

    # Split
    input = Config.getImagePathConfig(["output"])[0]
    output = Config.getImagePathConfig(["dataset"])[0]
    ImageUtil().splitTrainEval(input, output)
    """

    # Convertie les images dans un fichier .gz
    ConvertImageToMnistFormat()


    # Load training and eval data
    dateSets = CustomReadFile().read_data_sets()
    train_data = dateSets.train.images
    train_labels = np.asarray(dateSets.train.labels, dtype=np.int32)
    eval_data = dateSets.test.images  # Returns np.array
    eval_labels = np.asarray(dateSets.test.labels, dtype=np.int32)

    # Create the Estimator
    model_dir = Config.getDeepLearningConfig(["model_dir"])[0]
    mnist_classifier = tf.estimator.Estimator(model_fn=Model.cnn_model_fn, model_dir=model_dir)

    # Set up logging for predictions
    # Log the values in the "Softmax" tensor with label "probabilities"
    tensors_to_log = {"probabilities": "softmax_tensor"}
    logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=50)

    # Train the model
    train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": train_data},
        y=train_labels,
        batch_size=100,
        num_epochs=None,
        shuffle=True)

    mnist_classifier.train(
        input_fn=train_input_fn,
        steps=10000,
        hooks=[logging_hook])

    # Evaluate the model and print results
    eval_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"x": eval_data},
        y=eval_labels,
        num_epochs=1,
        shuffle=False)

    eval_results = mnist_classifier.evaluate(input_fn=eval_input_fn)
    print(eval_results)
    end = datetime.now()
    print("execution time : ", (end - begin))


if __name__ == "__main__":
  tf.app.run()
